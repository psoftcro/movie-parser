# Movie Parser

## Info

Adding movies to database and getting requested movies by some criteria.  Wrapper used in this project: **tmdbsimple** More info:  https://github.com/celiao/tmdbsimple/

## Quickstart
- `docker-compose up --build`

## Docs
http://localhost:5000/