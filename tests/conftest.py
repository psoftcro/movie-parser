import pytest

from alembic.command import upgrade as alembic_upgrade
from alembic.config import Config as AlembicConfig
from sqlalchemy_utils.functions import create_database, database_exists, drop_database

from src.app import configure_app, create_app
from src.config import SQLALCHEMY_DATABASE_URI
from src.modules.extensions import db as _db
from tests.db_seed import seed

ALEMBIC_CONFIG = "migrations/alembic.ini"
SQLALCHEMY_DATABASE_URI_TEST = f"{SQLALCHEMY_DATABASE_URI}_test"


def _apply_migrations():
    alembic_config = AlembicConfig(ALEMBIC_CONFIG)
    alembic_config.set_main_option("sqlalchemy.url", SQLALCHEMY_DATABASE_URI_TEST)
    alembic_upgrade(alembic_config, "head")


@pytest.fixture(scope="function")
def app(request):
    def teardown():
        ctx.pop()

    _app = create_app()
    _app.config["TESTING"] = True
    configure_app(_app)
    _app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI_TEST
    ctx = _app.app_context()
    ctx.push()
    request.addfinalizer(teardown)
    return _app


@pytest.fixture(scope="function")
def client(app):
    return app.test_client()


@pytest.fixture(scope="function")
def db(app, request):
    def teardown():
        drop_database(SQLALCHEMY_DATABASE_URI_TEST)

    _db.context = app.app_context
    if not database_exists(SQLALCHEMY_DATABASE_URI_TEST):
        create_database(SQLALCHEMY_DATABASE_URI_TEST)
    _apply_migrations()
    seed(_db.session)
    _db.app = app
    request.addfinalizer(teardown)
    return _db
