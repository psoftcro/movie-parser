from types import SimpleNamespace
from unittest.mock import patch

from src.db.movie import Movie


@patch("src.routes.tmdb.Movies")
def test_add_movie(mock_tmdb, db, client):
    with db.context():

        def movie_dict():
            return {
                "id": "126",
                "title": "Ok",
                "popularity": "1000",
                "vote_average": "6.5",
                "vote_count": 35000,
                "genres": [{"name": "Action"}],
                "poster_path": "asd2131ads.png",
                "overview": "The plot starts...",
                "release_date": "2020-12-30",
                "imdb_id": "aaa123bbb",
            }

        movie_id = "126"
        headers = {"Content-Type": "application/json"}
        data = {"movie_id": movie_id}
        movie = SimpleNamespace()
        movie.info = movie_dict
        mock_tmdb.return_value = movie
        response = client.post("/movies", json=data, headers=headers)
        res, code = response.json, response.status_code
        db_model = db.session.query(Movie).get(movie_id)
        assert (
            code == 201
            and db_model.movie_id == int(movie_id)
            and res.get("message") == "Created"
        )


def test_movie_exists(db, client):
    with db.context():
        movie_id = "603"
        headers = {"Content-Type": "application/json"}
        data = {"movie_id": movie_id}
        response = client.post("/movies", json=data, headers=headers)
        res, code = response.json, response.status_code
        db_model = db.session.query(Movie).get(movie_id)
        assert (
            code == 409
            and db_model.movie_id == int(movie_id)
            and res.get("message") == "Conflict"
        )


def test_invalid_format(db, client):
    with db.context():
        movie_id = "abc"
        headers = {"Content-Type": "application/json"}
        data = {"movie_id": movie_id}
        response = client.post("/movies", json=data, headers=headers)
        res, code = response.json, response.status_code
        assert code == 400 and res.get("message") == "Bad Request"


def test_get_movie_id(db, client):
    with db.context():
        response = client.get("/movies/123")
        code = response.status_code
        assert code == 200


def test_get_invalid_movie_id(db, client):
    with db.context():
        response = client.get("/movies/150")
        res, code = response.json, response.status_code
        assert (
            code == 404
            and res.get("description") == "`movie_id` not found"
            and res.get("message") == "Not Found"
        )


def test_get_invalid_movie_id_format(db, client):
    with db.context():
        response = client.get("/movies/a")
        res, code = response.json, response.status_code
        assert (
            code == 404
            and res.get("description") == "Invalid `movie_id` format"
            and res.get("message") == "Not Found"
        )


def test_get_requested_movies(db, client):
    with db.context():
        params = {"genres": "", "popularity": "10000", "vote_average": "", "title": ""}
        response = client.get("/movies", query_string=params)
        res, code = response.json, response.status_code
        assert code == 200 and len(res) == 2


def test_get_requested_movie(db, client):
    with db.context():
        params = {
            "genres": "",
            "popularity": "11000",
            "vote_average": "",
            "title": "lord of",
        }
        response = client.get("/movies", query_string=params)
        res, code = response.json, response.status_code
        assert code == 200 and len(res) == 1


def test_get_invalid_format(db, client):
    with db.context():
        params = {"genres": "1", "popularity": "", "vote_average": "", "title": ""}
        response = client.get("/movies", query_string=params)
        res, code = response.json, response.status_code
        assert code == 400 and res.get("message") == "Bad Request"


def test_get_invalid_movies(db, client):
    with db.context():
        params = {"genres": "", "popularity": "", "vote_average": "", "title": "no end"}
        response = client.get("/movies", query_string=params)
        res, code = response.json, response.status_code
        assert code == 404 and res.get("message") == "Not Found"
