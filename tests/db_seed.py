from src.db.movie import Movie


def seed(session):
    movie_data = [
        Movie(
            movie_id="603",
            title="Matrix",
            popularity="10000",
            vote_average="7",
            vote_count="3500",
            genres=[
                "Action",
                "Science Fiction",
            ],
            poster_path="movie312cool.png",
            overview="The plot...",
            release_date="2020-12-29",
            imdb_id="some3first",
        ),
        Movie(
            movie_id="123",
            title="Lord Of The Rings",
            popularity="15000",
            vote_average="8",
            vote_count="4500",
            genres=[
                "Fantasy",
                "Adventure",
            ],
            poster_path="movie2cooler.png",
            overview="The plot again...",
            release_date="2020-12-30",
            imdb_id="some3second",
        ),
    ]
    session.add_all(movie_data)
    session.commit()
