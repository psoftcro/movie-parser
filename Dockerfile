FROM python:3.8-alpine

COPY . /app
WORKDIR /app

RUN apk update && \
    apk add tzdata && \
    apk add --no-cache --virtual=.build-deps gcc libffi-dev build-base musl-dev postgresql-dev && \
    apk add --no-cache --virtual=.run-deps postgresql-libs postgresql-client && \
    pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt && \
    apk --purge del .build-deps

RUN cp /usr/share/zoneinfo/Europe/Zagreb /etc/localtime
RUN echo 'Europe/Zagreb' > /etc/timezone
ENV TZ='Europe/Zagreb'

CMD ['python', 'manage.py', 'runserver' ]
EXPOSE 5000
