from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.sql import func

from src.modules.extensions import db


class Movie(db.Model):
    __tablename__ = "movies"

    movie_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    popularity = db.Column(db.String(60))
    vote_average = db.Column(db.String(10))
    vote_count = db.Column(db.Integer)
    genres = db.Column(ARRAY(db.String))
    poster_path = db.Column(db.String(255))
    overview = db.Column(db.String(255))
    release_date = db.Column(db.DateTime)
    imdb_id = db.Column(db.String(20))
    created_at = db.Column(
        db.DateTime, nullable=False, default=func.now(), server_default=func.now()
    )
    updated_at = db.Column(db.DateTime, default=func.now(), server_default=func.now())

    def to_dict(self):
        return {
            "movie_id": self.movie_id,
            "title": self.title,
            "popularity": self.popularity,
            "vote_average": self.vote_average,
            "vote_count": self.vote_count,
            "genres": self.genres,
        }

    def to_dict_extended(self):
        return {
            "movie_id": self.movie_id,
            "title": self.title,
            "popularity": self.popularity,
            "vote_average": self.vote_average,
            "vote_count": self.vote_count,
            "genres": self.genres,
            "poster_path": self.poster_path,
            "overview": self.overview,
            "release_date": self.release_date,
            "imdb_id": self.imdb_id,
        }
