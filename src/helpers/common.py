def is_numeric(var):
    try:
        int(var)
        return True
    except ValueError:
        pass
    try:
        float(var)
        return True
    except ValueError:
        pass
    return False


def overview_check(overview_text):
    while len(overview_text) > 255:
        index = overview_text.rfind(" ")
        overview_text = overview_text[:index] + "..."
    return overview_text
