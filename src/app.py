from flask import Flask

from src.helpers.encoder import CustomJSONEncoder
from src.modules.extensions import configure_extensions
from src.modules.blueprints import configure_blueprints


def create_app():
    application = Flask(__name__)
    application.json_encoder = CustomJSONEncoder
    application.config.from_pyfile("config.py", silent=False)
    return application


def configure_app(application):
    configure_extensions(application)
    configure_blueprints(application)


app = create_app()
configure_app(app)
