import os


DEBUG = os.environ.get("DEBUG", "1")
ENV = os.environ.get("ENV", "development")
HOST = os.environ.get("HOST", "0.0.0.0")
PORT = int(os.environ.get("PORT", 5000))
SECRET_KEY = os.environ.get("SECRET_KEY", "12345678")

DB_DRIVER = os.environ.get("DB_DRIVER", "postgresql")
DB_HOST = os.environ.get("DB_HOST", "db")
DB_USER = os.environ.get("DB_USER", "movie_db_user")
DB_PASSWORD = os.environ.get("DB_PASSWORD", "movie_db_pass")
DB_DATABASE = os.environ.get("DB_DATABASE", "movie_db")
DB_PORT = int(os.environ.get("DB_PORT", 5432))
SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get(
    "SQLALCHEMY_TRACK_MODIFICATIONS", "false"
)
SQLALCHEMY_DATABASE_URI = (
    f"{DB_DRIVER}://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_DATABASE}"
)

TMDB_KEY = os.environ.get("TMDB_KEY", "12345")

SWAGGER_UI_VERSION = os.environ.get("SWAGGER_UI_VERSION", "3")
SWAGGER_TITLE = os.environ.get("SWAGGER_TITLE", "Movie Api")
SWAGGER = {
    "title": f"{SWAGGER_TITLE} Documentation",
    "uiversion": SWAGGER_UI_VERSION,
    "openapi": "3.0.0",
    "info": {
        "description": None,
        "termsOfService": None,
        "title": SWAGGER_TITLE,
        "version": "0.0.1",
    },
}
SWAGGER_ADDITIONAL = {
    "headers": [],
    "specs": [
        {
            "endpoint": "apispec_1",
            "route": "/apispec_1.json",
            "rule_filter": lambda rule: True,
            "model_filter": lambda tag: True,
        }
    ],
    "static_url_path": "/flasgger_static",
    "swagger_ui": True,
    "specs_route": "/",
}
