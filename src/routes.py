import tmdbsimple as tmdb
from flask import request, Blueprint
from requests.exceptions import HTTPError
from sqlalchemy import cast
from sqlalchemy.exc import DataError
from sqlalchemy.dialects.postgresql import ARRAY, FLOAT
from werkzeug.exceptions import BadRequest

from src.config import TMDB_KEY
from src.db.movie import Movie
from src.helpers.common import is_numeric, overview_check
from src.modules.extensions import db
from src.modules.responses import get_response

bp = Blueprint("root", __name__, url_prefix="/")
tmdb.API_KEY = TMDB_KEY


@bp.route("/movies", methods=["POST"])
def add():
    try:
        payload = request.json
    except BadRequest:
        return get_response(400, "Invalid payload")
    if "movie_id" not in payload:
        return get_response(400, "Param `movie_id` is required")
    if not is_numeric(payload["movie_id"]):
        return get_response(400, "Invalid `movie_id` format")
    exists = Movie.query.filter_by(movie_id=payload["movie_id"]).first()
    if exists:
        return get_response(409, "Movie already exists")
    movie = tmdb.Movies(payload["movie_id"])
    try:
        response = movie.info()
    except HTTPError:
        return get_response(404, "`movie_id` not found")
    db_movie = Movie()
    db_movie.movie_id = response["id"]
    db_movie.title = response["title"]
    db_movie.popularity = response["popularity"]
    db_movie.vote_average = response["vote_average"]
    db_movie.vote_count = response["vote_count"]
    genres = [g["name"] for g in response["genres"]]
    db_movie.genres = genres
    db_movie.poster_path = response["poster_path"]
    db_movie.overview = overview_check(response["overview"])
    db_movie.release_date = response["release_date"]
    db_movie.imdb_id = response["imdb_id"]
    db.session.add(db_movie)
    db.session.commit()
    return get_response(201)


@bp.route("/movies", methods=["GET"])
def grab():
    query_filters = []
    param_genres = request.args.get("genres")
    param_popularity = request.args.get("popularity")
    param_vote_average = request.args.get("vote_average")
    param_title = request.args.get("title")
    if param_genres:
        if is_numeric(param_genres):
            return get_response(400, "Invalid genre(s) format!")
        param_genres = param_genres.split(",")
        param_genres = [(p.strip()).title() for p in param_genres]
        query_filters.append(
            cast(Movie.genres, ARRAY(db.String)).contains(
                cast(param_genres, ARRAY(db.String))
            )
        )
    if param_popularity:
        if not is_numeric(param_popularity):
            return get_response(
                400,
                "Invalid popularity format, put whole number or with `.`(dot) separator!",
            )
        query_filters.append(
            cast(Movie.popularity, FLOAT()) >= cast(param_popularity, FLOAT())
        )
    if param_vote_average:
        if not is_numeric(param_vote_average):
            return get_response(
                400,
                "Invalid vote_average format, put whole number or with `.`(dot) separator!",
            )
        query_filters.append(
            cast(Movie.vote_average, FLOAT()) >= cast(param_vote_average, FLOAT())
        )
    if param_title:
        query_filters.append(Movie.title.ilike("%{0}%".format(param_title)))
    exists = Movie.query.filter(*query_filters).all()
    if exists:
        return get_response(200, params=[e.to_dict() for e in exists])
    return get_response(404)


@bp.route("/movies/<movie_id>", methods=["GET"])
def data(movie_id):
    if not is_numeric(movie_id):
        return get_response(404, "Invalid `movie_id` format")
    try:
        exists = Movie.query.filter(Movie.movie_id == int(movie_id)).first()
        if exists:
            return get_response(200, params=exists.to_dict_extended())
    except DataError:
        pass
    return get_response(404, "`movie_id` not found")
