from flask import jsonify

messages = {
    200: "Success",
    201: "Created",
    400: "Bad Request",
    401: "Unauthorized",
    403: "Forbidden",
    404: "Not Found",
    409: "Conflict",
    429: "Too Many Requests",
    500: "Internal Server Error",
}


def get_response(code, description=None, params=None):
    message = messages.get(code)
    message = "Undefined" if not message else message
    code = 0 if message == "Undefined" else code
    data = {"code": code, "message": message}

    if description:
        data["description"] = description

    if params:
        data = {**data, **params} if code != 200 else params

    return jsonify(data), code
