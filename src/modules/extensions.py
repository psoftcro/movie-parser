from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flasgger import Swagger

from src.config import SWAGGER_ADDITIONAL

db = SQLAlchemy()
migrate = Migrate()
swagger = Swagger(template_file="docs/docs.yaml", config=SWAGGER_ADDITIONAL)


def configure_extensions(app):
    db.init_app(app)
    migrate.init_app(app, db)
    swagger.init_app(app)
