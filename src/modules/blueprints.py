from src import routes

active_blueprints = (routes,)


def configure_blueprints(app):
    for blueprint in active_blueprints:
        app.register_blueprint(blueprint.bp)
